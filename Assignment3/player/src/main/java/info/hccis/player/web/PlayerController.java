package info.hccis.player.web;

import info.hccis.player.dao.PlayerDAO;
import info.hccis.player.entity.Player;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PlayerController {

    @RequestMapping("player/add")
    public String playerAdd(Model model) {
        System.out.println("BJM-in controller for /player/add");
        Player temp = new Player();
        model.addAttribute("player", temp);

        return "player/add";
    }
    
    @RequestMapping("player/edit")
    public String playerAddSubmit(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
       //get the player from the db
        Player player = PlayerDAO.select(id);
        //send the user back to the add page after putting the camper object in the model
        model.addAttribute("player", player);
        return "player/add";
    }

    @RequestMapping("player/addSubmit")
    public String playerAddSubmit(Model model, @ModelAttribute("player") Player player) {
        if (player.getAmountPaid() > 50){
                System.out.println("The amount paid must be less than 50.");
            }
        else{ try {
            //System.out.println("player name =" + player.getFirstName());

            PlayerDAO.update(player);

            //send to the list of players view.
            ArrayList<Player> thePlayers = PlayerDAO.selectAll();
            model.addAttribute("thePlayers", thePlayers);

            return "player/list";
        } catch (Exception ex) {
            System.out.println("There was an error adding the player.");
            return "player/list";
        }
        }
        model.addAttribute("invalidAmountPaid" , "The amount paid is greater than the maximum of 50.");
        return "player/add";
    }
   
}
