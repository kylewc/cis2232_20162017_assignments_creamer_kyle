package info.hccis.player.util;

import java.security.*;
import java.math.*;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * General program utilities
 * @author bjmaclean
 * @since 20150918
 */
public class Utility {
    private static Scanner input = new Scanner(System.in);

    public static Scanner getInput() {
        return input;
    }
    
    public static String getHashPassword(String inPassword){
        try {
            return getMD5Hash(inPassword);
        } catch (Exception ex) {
            Logger.getLogger(Utility.class.getName()).log(Level.SEVERE, null, ex);
        }
        return "Should not see this!";
    }
    

    public static String getMD5Hash(String passwordIn) throws Exception{
        MessageDigest m=MessageDigest.getInstance("MD5");
        m.update(passwordIn.getBytes(),0,passwordIn.length());
        return ""+new BigInteger(1,m.digest()).toString(16);
    }
}
    
    

