package info.hccis.player.web;

import info.hccis.player.dao.PlayerDAO;
import info.hccis.player.entity.Player;
import java.util.ArrayList;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    
    @RequestMapping("/")
    public String showHome(Model model) {
        System.out.println("in controller for /");

        ArrayList<Player> players = PlayerDAO.selectAll();
        System.out.println("Number of player = " + players.size());
        model.addAttribute("thePlayers", players);
        double totalPaid = 0;
        for(Player player : players){
            totalPaid += player.getAmountPaid();
        }
        model.addAttribute("totalPaid", totalPaid);
        return "player/list";
    }
    
    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "player/newOne";
        
        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/player/
        */
    }

}
