create database squash;
use squash;

/*create a user in database*/
grant select, insert, update, delete, alter on squash.*
to 'cis2232_admin'@'localhost'
identified by 'Test1234';
flush privileges;


-- May be included after database is created to clear database.
-- delete from Camper;
-- drop table Camper;
-- 
-- delete from CodeValue;
-- delete from UserAccess;
-- delete from CodeType;
-- 
-- drop table UserAccess;
-- drop table CodeValue;
-- drop table CodeType;




-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 01, 2016 at 01:18 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";




/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `players`
--

-- --------------------------------------------------------

--
-- Table structure for table `camper`
--

CREATE TABLE `camper` (
  `id` int(4) UNSIGNED NOT NULL COMMENT 'Registration id',
  `firstName` varchar(10) DEFAULT NULL,
  `lastName` varchar(10) DEFAULT NULL,
  `dob` varchar(10) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `camper`
--

INSERT INTO `camper` (`id`, `firstName`, `lastName`, `dob`) VALUES
(1, 'Nick', 'Matheson', '12345'),
(2, 'test', 'test', 'test'),
(3, 'asdf', 'asdf', 'asdf'),
(4, 'asdgfasdg', 'asdgasdg', 'asdgash'),
(5, 'asdgasdg', 'asdgasdg', 'asdgasdf'),
(6, 'tester3', 'tester3', 'tester3'),
(7, 'zxcb', 'zxcbv', 'zxbc'),
(8, 'ET', 'ET', 'ET'),
(9, 'Q', 'Q', 'Q'),
(10, 'qrewt', 'qwet', 'qwer'),
(11, 'wasd', 'wasd', 'wasd'),
(12, 'wee', 'wee', 'wee');

-- --------------------------------------------------------

--
-- Table structure for table `codetype`
--

CREATE TABLE `codetype` (
  `CodeTypeId` int(3) NOT NULL COMMENT 'This is the primary key for code types',
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This hold the code types that are available for the applicat';

--
-- Dumping data for table `codetype`
--

INSERT INTO `codetype` (`CodeTypeId`, `englishDescription`, `frenchDescription`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 'User Types', 'User Types FR', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '');

-- --------------------------------------------------------

--
-- Table structure for table `codevalue`
--

CREATE TABLE `codevalue` (
  `codeTypeId` int(3) NOT NULL COMMENT 'see code_type table',
  `codeValueSequence` int(3) NOT NULL,
  `englishDescription` varchar(100) NOT NULL COMMENT 'English description',
  `englishDescriptionShort` varchar(20) NOT NULL COMMENT 'English abbreviation for description',
  `frenchDescription` varchar(100) DEFAULT NULL COMMENT 'French description',
  `frenchDescriptionShort` varchar(20) DEFAULT NULL COMMENT 'French abbreviation for description',
  `createdDateTime` datetime DEFAULT NULL,
  `createdUserId` varchar(20) DEFAULT NULL,
  `updatedDateTime` datetime DEFAULT NULL,
  `updatedUserId` varchar(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='This will hold code values for the application.';

--
-- Dumping data for table `codevalue`
--

INSERT INTO `codevalue` (`codeTypeId`, `codeValueSequence`, `englishDescription`, `englishDescriptionShort`, `frenchDescription`, `frenchDescriptionShort`, `createdDateTime`, `createdUserId`, `updatedDateTime`, `updatedUserId`) VALUES
(1, 1, 'General', 'General', 'GeneralFR', 'GeneralFR', '2015-10-25 18:44:37', 'admin', '2015-10-25 18:44:37', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `player`
--

CREATE TABLE `player` (
  `id` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `parentName` varchar(20) NOT NULL,
  `phoneNumber` varchar(20) NOT NULL,
  `emailAddress` varchar(30) NOT NULL,
  `amountPaid` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `player`
--

INSERT INTO `player` (`id`, `name`, `parentName`, `phoneNumber`, `emailAddress`, `amountPaid`) VALUES
(1, 'Ted', 'Fred', '9029407256', 'ted@fred.com', 12),
(2, 'test', 'test', 'test', 'test', 15),

-- --------------------------------------------------------

--
-- Table structure for table `useraccess`
--

CREATE TABLE `useraccess` (
  `userAccessId` int(3) NOT NULL,
  `username` varchar(100) NOT NULL COMMENT 'Unique user name for app',
  `password` varchar(128) NOT NULL,
  `userTypeCode` int(3) NOT NULL DEFAULT '1' COMMENT 'Code type #1',
  `createdDateTime` datetime DEFAULT NULL COMMENT 'When user was created.'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `camper`
--
ALTER TABLE `camper`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `codetype`
--
ALTER TABLE `codetype`
  ADD PRIMARY KEY (`CodeTypeId`);

--
-- Indexes for table `codevalue`
--
ALTER TABLE `codevalue`
  ADD PRIMARY KEY (`codeValueSequence`),
  ADD KEY `codeTypeId` (`codeTypeId`);

--
-- Indexes for table `player`
--
ALTER TABLE `player`
  ADD KEY `id` (`id`);

--
-- Indexes for table `useraccess`
--
ALTER TABLE `useraccess`
  ADD PRIMARY KEY (`userAccessId`),
  ADD KEY `userTypeCode` (`userTypeCode`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `codetype`
--
ALTER TABLE `codetype`
  MODIFY `CodeTypeId` int(3) NOT NULL AUTO_INCREMENT COMMENT 'This is the primary key for code types', AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `codevalue`
--
ALTER TABLE `codevalue`
  MODIFY `codeValueSequence` int(3) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `useraccess`
--
ALTER TABLE `useraccess`
  MODIFY `userAccessId` int(3) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `codevalue`
--
ALTER TABLE `codevalue`
  ADD CONSTRAINT `codevalue_ibfk_1` FOREIGN KEY (`codeTypeId`) REFERENCES `codetype` (`CodeTypeId`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
