/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.squashregistrationapp;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.OutputStream;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author: Kyle Creamer
 * @date: September 26th, 2016
 * @purpose:  This application is for registering squash players.  Players are 
 * loaded from a file using json (jackson) to an ArrayList and can be added to, shown,
 * or have their payment amount modified.
 */
public class SquashRegistrationMain {
    
    public static final String FILE_PATH = "c:\\cis2232\\players.json";
    
    public static void main(String[] args) throws FileNotFoundException, IOException {
        
       
        //Creating ArrayList of Players
        ArrayList<Player> playersList = new ArrayList<>();
        
        //Assigning values from saved file if already existing playerList file is found.
        try {
            playersList = doesFileExist(playersList);
        } catch (IOException ex) {
            Logger.getLogger(SquashRegistrationMain.class.getName()).log(Level.SEVERE, null, ex);
        }


        boolean finished = false;
        //Scanner for input from the user.
        Scanner input = new Scanner(System.in);

        //Prompt to be used for the menu.
        String prompt = "\nEnter menu choice:"
                + "\nA) Add Player"
                //Show all of the player details to the console.
                + "\nS) Show Players"
                //Allow the user to update the amount paid for any player in the ArrayList created above. 
                //After updating a pay amount show the new total amount paid by all players.
                + "\nG) Update amount paid."
                + "\nX) Exit";
        //String to hold user's choice
        String userChoice;
        //while the user is not finished, continue to prompt them with the menu.
        do {
                System.out.println(prompt);
                userChoice = input.nextLine().toUpperCase();
                //This switch statement holds our menu choices.
                switch (userChoice) {
                    case "A":
                        //We add a new player to the arraylist.
                        playersList.add(new Player(input));
                        break;
                    case "S":
                        //We loop through the arraylist to display details of each player.
                        for (Player player : playersList){
                            System.out.println(player.toString());
                        }
                        break;
                    case "G":
                        //We search for a plyer to update the amount paid, matching by name.
                        System.out.println("Which player (name) would you like to update an amount for?");
                        for (Player player : playersList){
                            System.out.println(player.getName());
                        }
                      
                        String selectedPlayer = input.nextLine().toLowerCase();
                        int i = 0;
                        int totalAmountPaid = 0;
                        for (Player player : playersList){
                            //IF the name matches we update the input amount.
                            if(player.getName().toLowerCase().equals(selectedPlayer)){
                                System.out.println("What is the new amount?");
                                
                                playersList.get(i).setAmountPaid(input.nextDouble());
                                input.nextLine();
                                
                            }
                            else{
                                //If a player is not located we output an error.
                                ++i;
                                if (i == playersList.size()){
                                    System.out.println("A player was not located by that name.");
                                }
                            }
                            //We display the total amount paid if at the end of the arrayList
                            if (i == playersList.size()-1){
                            totalAmountPaid += playersList.get(i).getAmountPaid();
                                System.out.println("\nTotal amount paid: " + totalAmountPaid);

                        }
                        }
                        //We display the total amount paid for all players inclduing the updated amount.
                        System.out.println("The new total amount paid for all players is:");
                        for (Player player : playersList){
                            System.out.println("Player Name: " + player.getName() + "\nPaid Amount: " + player.getAmountPaid());
                            
                        }
                        break;
                    case "X":
                        //Jackson Method
                        //We create an output stream to write to using Jackson's writeValue method.
                            ObjectMapper mapper = new ObjectMapper();
                           OutputStream out = new FileOutputStream(FILE_PATH);
                            mapper.writeValue(out, playersList);
                        System.out.println("Finished.");
                        
                        //Gson method
//                        Type type =  new TypeToken<ArrayList<Player>>(){}.getType();
//                        BufferedReader fileReader gson.toJson(playerList, type);
                        finished = true;
                        break;
                       
                        //If an invalid choice is made we inform the user.
                        default:
                        System.out.println("Invalid choice!");
                        break;
                }
                
            
        } while (finished == false);
        System.exit(0);
    }
    
    /*
    @author: Kyle Creamer
    @date: September 26th, 2016
    @purpose: This method will check if the players.txt file exists and create
    an arraylist from it.  If it does not exist, the initial ArrayList is returned
    unmodified.
    */
    public static ArrayList<Player> doesFileExist(ArrayList<Player> playersList) throws IOException{
                //creating filereader for Jackson to use.
                BufferedReader fileReader = new BufferedReader(new FileReader(FILE_PATH));
                File file = new File(FILE_PATH);
                //If the file exists we will try to load the arraylist.
            if(file.exists()){
                ObjectMapper mapper = new ObjectMapper();
            try {
                //Creating a type for Jackson to understand what it is deserializing.
                TypeReference type = new TypeReference<ArrayList<Player>>(){};
                ArrayList<Player> tempList;
                tempList = mapper.readValue(fileReader, type);
                //We return an empty or filled arrayList.
                playersList = tempList;
                return playersList;
            } catch (FileNotFoundException ex) {
                Logger.getLogger(SquashRegistrationMain.class.getName()).log(Level.SEVERE, null, ex);
            }
                }
                //If the file does not exist, we create it.
        else{
            File newFile = new File("c:\\cis2232\\players.json");
            if(newFile.mkdirs()){
                System.err.println("Fle was not found, but directory has been created.");
            }
        }
                //We return the arraylist if the tempList is not empty.
        return playersList;
    }
}