/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.squashregistrationapp;

import java.util.Scanner;

/**
 *
 * @author Kyle
 * @date September 26th, 2016
 * @purpose This class holds data pertaining to Squash Players being registered.
 */
public class Player {
        
    private String name = "";
    private String parentName = "";
    private String phone = "";
    private String email = "";
    private double amountPaid = 0;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getParentName() {
        return parentName;
    }

    public void setParentName(String parentName) {
        this.parentName = parentName;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public double getAmountPaid() {
        return amountPaid;
    }

    public void setAmountPaid(double amountPaid) {
        this.amountPaid = amountPaid;
    }
    
    /*
    @author Kyle Creamer
    @date September 26th, 2016
    @purpose We create a Player by sending an input to it, so that it can be serialized later.
    A Scanner object can not be stored in this class as it willbreak serialization.
    */
    public Player(Scanner input){
        System.out.println("What is the player's name?");
        this.name = input.nextLine();
        System.out.println("What is the player's parent's name?");
        this.parentName=input.nextLine();
        System.out.println("What is the player's phone number?");
        this.phone = input.nextLine();
        System.out.println("What is the player's e-mail address?");
        this.email = input.nextLine();
        System.out.println("What is the amount paid for the player?");
        this.amountPaid = input.nextDouble();
        input.nextLine();
    }
    
    public Player(){
        
    }
    
    @Override
    /*
    @Author Kyle Creamer
    @date September 26th, 2016
    @purpose We override the default toString for our Player to allow easier
    reading for users.
    */
    public String toString(){
        String output = "\nPlayer Name: " + this.name ;
        output += "\nPlayer Parent's Name: " + this.parentName;
        output += "\nPlayer's E-mail Address: " + this.email;
        output += "\nPlayer's Phone Number: " + this.phone;
        output += "\nPlayer's amount Paid: " + this.amountPaid;
        return output;
    }
}
