package info.hccis.player.dao;

import info.hccis.player.util.Utility;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ResourceBundle;

public class UserAccessDAO {

    public static String getUserTypeCode(String database, String username, String password) {
        Connection conn = null;
        PreparedStatement ps = null;
        String sql;

        try {
            //connect to database
            String propFileName = "spring.data-access";
            ResourceBundle rb = ResourceBundle.getBundle(propFileName);
            String dbUserName = rb.getString("jdbc.username");
            String dbPassword = rb.getString("jdbc.password");
            System.out.println("BJM-Set the database to "+database);

            conn = ConnectionUtils.getConnection();
            
            //create query to get usertypecode
            sql = "SELECT `userTypeCode` FROM `UserAccess` WHERE `username` = '" + username + "' AND `password`= '" + Utility.getHashPassword(password) + "'";
            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();

            //get data if statement returns a value
            String userTypeCode = null;
            while (rs.next()) {
                userTypeCode = Integer.toString(rs.getInt("userTypeCode"));
            }

            //return if data found
            if (userTypeCode != null) {
                return userTypeCode;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            System.out.println("ERROR GETTING: User Type Code"
                    + "WHY: " + errorMessage);
        } finally {
            DbUtils.close(ps, conn);
        }
        //if data isn't found return 0
        return "0";
    }
}