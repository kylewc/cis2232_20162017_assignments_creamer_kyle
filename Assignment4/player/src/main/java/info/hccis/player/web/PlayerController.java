package info.hccis.player.web;

import info.hccis.player.data.springjpa.PlayerRepository;
import info.hccis.player.entity.Player;
import java.util.ArrayList;
import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class PlayerController {

    private final PlayerRepository playerRepository;

    @Autowired
    public PlayerController(PlayerRepository playerRepository) {
        this.playerRepository = playerRepository;
    }

    @RequestMapping("player/add")
    public String playerAdd(Model model) {
        System.out.println("BJM-in controller for /player/add");
        Player temp = new Player();
        model.addAttribute("player", temp);

        return "player/add";
    }

    @RequestMapping("player/edit")
    
    public String playerAddSubmit(Model model, HttpServletRequest request) {
        Integer id = Integer.parseInt(request.getParameter("id"));
        //get the player from the db
        Player player = playerRepository.findOne(id);
        //send the user back to the add page after putting the camper object in the model
        model.addAttribute("player", player);
        return "player/add";
    }

    @RequestMapping("player/addSubmit")
    public String playerAddSubmit(Model model, @Valid @ModelAttribute("player") Player player, BindingResult result) {
        String error = "";
        
        if (result.hasErrors()) {
            error = "The amount paid must be greater than 0 and less than 50.";
            model.addAttribute("invalidAmountPaid", error);
            return "player/add";
        } else {
            //System.out.println("player name =" + player.getFirstName());
            try {
                playerRepository.save(player);
                ArrayList<Player> players = (ArrayList<Player>) playerRepository.findAll();
                model.addAttribute("thePlayers", players);
                double totalPaid = 0;
                for (Player eachPlayer : players) {
                    totalPaid += eachPlayer.getAmountPaid();
                }
                model.addAttribute("totalPaid", totalPaid);
            } catch (Exception ex) {
                error = "Error: " + ex;
                model.addAttribute("error", error);
                return "player/error";
            }
            return "player/list";
        }
    }
}
