/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cis2232_assignment6_creamer_kyle;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Kyle
 * @Date December 10th, 2016
 * @Purpse Test application for multithreading via console and gui to write to a synchronized file,
 * and to test Unit tests.
 */
public class main {

    public static void main(String[] args) {

        //We specify two threads - one to use gui, and one to use console.
        Thread thread1 = new Thread(new GUIRunner());
        thread1.start();
       
        Thread thread2 = new Thread(new ConsoleRunner());
        thread2.start();
    }

    public static void enterGUI() {
        //When entering the gui we enter the details GUI method.
        Lesson thisLesson = new Lesson();
        thisLesson.enterDetailsGUI();
        writeToFile(thisLesson.outputLesson(thisLesson));
    }

    public static void enterConsole() {
        //when entering console we enter the console method.
        Lesson thisLesson = new Lesson();
        thisLesson.enterDetails();
        writeToFile(thisLesson.outputLesson(thisLesson));
    }

    /*
    
    @Author Kyle Creamer
    @Date December 10th, 2016
    @Purpose:  We create a given file if it does not exist - once it exists
    we write the content of our to string for our Lesson object to the file.
    The method is synchronized so that two threads may write to it sequentially and not
    overwrite the file.
    */
    private synchronized static void writeToFile(String content) {
        File l = new File("lessons.txt");
        if (!l.exists()) {
            try {
                l.createNewFile();
            } catch (IOException ex) {
                Logger.getLogger(main.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
          BufferedWriter bw = null;
        try {
            bw = new BufferedWriter(new FileWriter("lessons.txt", true));
            bw.write(content);
            bw.newLine();
            bw.flush();
        } catch (IOException ioe) {
            ioe.printStackTrace();
        } finally {                       // always close the file
            if (bw != null) {
                try {
                    bw.close();
                } catch (IOException ioe2) {
                    // just ignore it
                }
            }

        }
    }
}
