/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cis2232_assignment6_creamer_kyle;

import java.util.Scanner;
import javax.swing.JOptionPane;

/**
 *
 * @author Kyle
 * @Date December 10th, 2016
 * @Purpose:  This object stores our main functionality for modifying itself and storing
 * details pertaining to a group tennis lesson booking.
 */
public class Lesson {

    private int cost;
    private String member;
    private int rate;
    private int hours;
    private int groupSize;

    //Constant variables to store cost per hour rates.
    private static final int PRIVATE_MEMBER_COST = 55;
    private static final int TWO_MEMBER_COST = 30;
    private static final int THREE_MEMBER_COST = 21;
    private static final int FOUR_MEMBER_COST = 16;
    private static final int PRIVATE_NONMEMBER_COST = 60;
    private static final int TWO_NONMEMBER_COST = 33;
    private static final int THREE_NONMEMBER_COST = 23;
    private static final int FOUR_NONMEMBER_COST = 18;

    public boolean valid = false;

    public Lesson() {
    }

    public int getHours() {
        return hours;
    }

    public void setHours(int hours) {
        this.hours = hours;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }

    public String getMember() {
        return member;
    }

    public void setMember(String member) {
        this.member = member;
    }

    public int getRate() {
        return rate;
    }

    public void setRate(int rate) {
        this.rate = rate;
    }

    public int getGroupSize() {
        return groupSize;
    }

    public void setGroupSize(int groupSize) {
        this.groupSize = groupSize;
    }

    //We set the group size, member status, and hours for the group and then
    //calculate its rate and output the lesson via console.
    public void enterDetails() {

        this.setGroupSize(enterGroupSize());
        this.setMember(enterMember());
        this.setHours(enterHours());
        setRate(this);
        System.out.println(outputLesson(this));
    }

    //We set the group size, member status, and hours for the group and then
    //calculate its rate and output the lesson via GUI
    public void enterDetailsGUI() {

        this.setGroupSize(enterGroupSizeGUI());
        this.setMember(enterMemberGUI());
        this.setHours(enterHoursGUI());
        setRate(this);
        JOptionPane.showMessageDialog(null, outputLesson(this));
    }

    //We can validate the group size to determine if it is valid (true) when 
    //between 1 and 4.
    public boolean validateGroupSize(int groupSize) {
        if (groupSize <= 0 || groupSize >= 5) {
            System.out.println("Invalid Group Size Entry");
            return false;
        }
        return true;
    }
    //We can validate the group size to determine if it is valid (true) when 
    //a Y or N value.
    public boolean validateMember(String member) {
        if (member.toLowerCase().equals("y") || member.toLowerCase().equals("n")) {
            return true;
        } else {
            System.out.println("Invalid Member Entry");
            return false;

        }
    }

    //We can validate the group size to determine if it is valid (true) when 
    //greater than 0.
    public boolean validateHours(int hours) {

        if (hours <= 0) {
            System.out.println("Invalid Hours Entry");
            return false;
        }

        return true;
    }
    
    /*
    @Author: Kyle Creamer
    @Date: Decembr 10th, 2016
    @Purpose:  This method accepts a lesson, and validates the member, groupsize,
    and hours variables.  It also will set the given rate and cost for a tennis lesson
    based on valid variables.
    
    
    */
    public void setRate(Lesson thisLesson) {

        try {
            if (thisLesson.getMember().toUpperCase().equals("Y")) {
                switch (thisLesson.getGroupSize()) {
                    case 1:
                        this.setRate(PRIVATE_MEMBER_COST);
                        break;
                    case 2:
                        this.setRate(TWO_MEMBER_COST);
                        break;
                    case 3:
                        this.setRate(THREE_MEMBER_COST);
                        break;
                    case 4:
                        this.setRate(FOUR_MEMBER_COST);
                        break;
                    default:
                        System.out.println("Invalid Group Size");
                        throw new RuntimeException();

                }

            } else if (thisLesson.getMember().toUpperCase().equals("N")) {
                switch (thisLesson.getGroupSize()) {
                    case 1:
                        this.setRate(PRIVATE_NONMEMBER_COST);
                        break;
                    case 2:
                        this.setRate(TWO_NONMEMBER_COST);
                        break;
                    case 3:
                        this.setRate(THREE_NONMEMBER_COST);
                        break;
                    case 4:
                        this.setRate(FOUR_NONMEMBER_COST);
                        break;
                    default:
                        System.out.println("Invalid Group Size");
                        throw new RuntimeException();

                }
            }
        } catch (Exception ex) {
            System.out.println("Invalid Member Entry");
            throw new RuntimeException();
        }
        try {
            validateHours(this.getHours());
        } catch (Exception ex) {
            System.out.println("Improper hours format");
            throw new RuntimeException();
        }
        this.setCost((this.getHours() * this.getRate()));
    }

    //We input the group size for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public int enterGroupSize() {

        valid = false;
        while (valid == false) {
            valid = false;
            Scanner input = new Scanner(System.in);
            System.out.println("Welcome to the CIS Tennis Program\n"
                    + "\n"
                    + "How many are in the group (1,2,3,4)");
            try {
                groupSize = input.nextInt();
                input.nextLine();
                try {
                    valid = validateGroupSize(groupSize);
                } catch (Exception ex) {
                    System.out.println("Invalid group size");

                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }

        }
        return groupSize;
    }
    
     //We input the group size for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public int enterGroupSizeGUI() {

        valid = false;
        while (valid == false) {
            valid = false;
            Scanner input = new Scanner(System.in);

            try {
                groupSize = Integer.parseInt(JOptionPane.showInputDialog("Welcome to the CIS Tennis Program\n"
                        + "\n"
                        + "How many are in the group (1,2,3,4)"));
                try {
                    valid = validateGroupSize(groupSize);
                } catch (Exception ex) {
                    System.out.println("Invalid group size");

                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }

        }
        return groupSize;
    }

     //We input the member type for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public String enterMember() {

        valid = false;
        while (valid == false) {
            valid = false;
            Scanner input = new Scanner(System.in);
            System.out.println("\nAre you a member (Y/N)?");

            try {
                member = input.nextLine();
                try {
                    valid = validateMember(member);
                } catch (Exception ex) {
                    System.out.println("Invalid entry");
                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }
        }
        return member;
    }

      //We input the member type for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public String enterMemberGUI() {

        valid = false;
        while (valid == false) {
            valid = false;
            try {
                member = JOptionPane.showInputDialog("Are you a member (Y/N)?");
               try {
                    valid = validateMember(member);
                } catch (Exception ex) {
                    System.out.println("Invalid entry");
                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }
        }
        return member;
    }

      //We input the amount of hours for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public int enterHoursGUI() {

        valid = false;
        while (valid == false) {
            valid = false;
            try {
                hours = Integer.parseInt(JOptionPane.showInputDialog("How many hours do you want for your lesson?\n"));
                try {
                    valid = validateHours(hours);
                } catch (Exception ex) {
                    System.out.println("Invalid entry");
                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }

        }
        return hours;
    }

      //We input the amount of hours for a tennis lesson via console and validate
    //the group size value, and if invalid we allow for re-entry.
    public int enterHours() {
        valid = false;
        while (valid == false) {
            valid = false;
            Scanner input = new Scanner(System.in);
            System.out.println("How many hours do you want for your lesson?\n");
            try {
                hours = input.nextInt();
                input.nextLine();
                try {
                    valid = validateHours(hours);
                } catch (Exception ex) {
                    System.out.println("Invalid entry");
                }
            } catch (Exception ex) {
                System.out.println("Invalid Format");
            }

        }
        return hours;
    }

    //This would be better as a toString, but I consider it output.
    public String outputLesson(Lesson thisLesson) {
        return ("Lesson Details - Lesson group size:" + thisLesson.getGroupSize() + " Member: " + thisLesson.getMember() + " Rate: $" + thisLesson.getRate() + "/hour Cost: $" + thisLesson.getCost());
    }

}
