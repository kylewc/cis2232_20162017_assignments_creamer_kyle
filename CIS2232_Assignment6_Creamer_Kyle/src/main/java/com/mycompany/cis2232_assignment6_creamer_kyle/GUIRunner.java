/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.cis2232_assignment6_creamer_kyle;

/**
 *
 * @author Kyle
 * @Date December 10th, 2016
 * @Purpose:  This thread allows for us to call the enterGUI method to add input
 * via the GUI.
 */
public class GUIRunner extends Thread{
    
    @Override
    public void run() {
           main.enterGUI();

            try {
                Thread.sleep(100);
            } catch (InterruptedException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
        }
        
    }
    
