/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.cis2232_assignment6_creamer_kyle.Lesson;
import com.mycompany.cis2232_assignment6_creamer_kyle.main;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author Kyle
 */
public class unittest {
    
    public unittest() {
    }
    private static Lesson ls;

    @BeforeClass
    public static void setUpClass() {
        ls = new Lesson(); 
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void whenHoursAreZero() {     
            boolean testZero = ls.validateHours(0);
            assert testZero == false;
        }
    

    @Test
    public void whenMemberIsNotY() {
            boolean testNotYorN = ls.validateMember("Yes");
            assert testNotYorN == false;
    }
    
    @Test
    public void whenGroupSize5(){
        boolean groupSizeOver5 = ls.validateGroupSize(5);
        assert groupSizeOver5 == false;
    }
    
    @Test
    public void whenSetRateMembersInvalidFormat(){
        boolean exceptionExists = false;
        try{
            ls.setMember("Yes");
            ls.setHours(2);
            ls.setGroupSize(2);
        ls.setRate(ls);
        }
        catch(Exception ex)
        {
            exceptionExists = true;
        }
        
        assert exceptionExists = true;
    }
    
        @Test
    public void whenSetRateGroupSizeInvalidSize(){
        boolean exceptionExists = false;
        try{
            ls.setMember("Y");
            ls.setHours(2);
            ls.setGroupSize(5);
        ls.setRate(ls);
        }
        catch(Exception ex)
        {
            exceptionExists = true;
        }
        
        assert exceptionExists = true;
    }

    
            @Test
    public void whenSetRateHoursAreZero(){
        boolean exceptionExists = false;
        try{
            ls.setMember("Y");
            ls.setHours(0);
            ls.setGroupSize(4);
        ls.setRate(ls);
        }
        catch(Exception ex)
        {
            exceptionExists = true;
        }
        
        assert exceptionExists = true;
    }
    
    // TODO add test methods here.
    // The methods must be annotated with annotation @Test. For example:
    //
    // @Test
    // public void hello() {}
}
