package playerClient;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2016-11-27T15:24:46")
@StaticMetamodel(Player.class)
public class Player_ { 

    public static volatile SingularAttribute<Player, String> parentName;
    public static volatile SingularAttribute<Player, String> emailAddress;
    public static volatile SingularAttribute<Player, String> phoneNumber;
    public static volatile SingularAttribute<Player, Integer> amountPaid;
    public static volatile SingularAttribute<Player, String> name;
    public static volatile SingularAttribute<Player, Integer> id;

}