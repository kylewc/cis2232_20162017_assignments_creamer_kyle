package playerClient;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.codehaus.jackson.JsonFactory;
import org.codehaus.jackson.JsonParser;
import org.codehaus.jackson.JsonToken;
import org.codehaus.jackson.map.ObjectMapper;

/**
 *
 * @author bjmaclean
 */
public class RestMethods {

    final public static String REST_IP = "127.0.0.1";

    /**
     * This method will access the CamperService to get all of the campers from
     * the campers web application service.
     *
     * @since 20161115
     * @author BJM
     */
    public static void listPlayersUsingRest() {

        //*********************************************
        //Access the rest web service
        //https://www.tutorialspoint.com/restful/restful_quick_guide.htm
        //*********************************************
        String output = "";
        try {

            URL url = new URL("http://" + REST_IP + ":8080/squash/api/PlayerService/players");
            HttpURLConnection conn = (HttpURLConnection) url.openConnection();
            conn.setDoOutput(true);
            conn.setRequestMethod("GET");
            conn.setRequestProperty("Content-Type", "application/json;charset=utf-8");

            //**********************************************************
            //Check to see that the connection is ok
            //**********************************************************
            if (conn.getResponseCode() != HttpURLConnection.HTTP_OK) {
                System.out.println("Failed : HTTP error code : "
                        + conn.getResponseCode());
            } else {
//                System.out.println("response="+conn.getResponseCode()+" "+conn.getResponseMessage());
                //Read the json back from the connection
                BufferedReader br = new BufferedReader(new InputStreamReader(
                        (conn.getInputStream())));

//                System.out.println("Success : Output from Server .... \n");
                String tempOutput = "";
                while ((tempOutput = br.readLine()) != null) {
                    output += tempOutput;
                }
                //**********************************************************
                //Output this to the console.  Notice that all of the campers are 
                //in json array.  The code below will break these into individual 
                //objects.
                //**********************************************************
//                System.out.println(output);

            }
            conn.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        //**********************************************************
        //Have to convert this json array to camper objects.
        //**********************************************************
        ObjectMapper mapper = new ObjectMapper();

        //**********************************************************
        //Code syntax found at:
        //http://stackoverflow.com/questions/7634518/getting-jsonobject-from-jsonarray
        //**********************************************************
        try {
            JsonFactory f = new JsonFactory();
            JsonParser jp = f.createJsonParser(output);
            // advance stream to START_ARRAY first:
            jp.nextToken();
            // and then each time, advance to opening START_OBJECT
            while (jp.nextToken() == JsonToken.START_OBJECT) {
                Player player = mapper.readValue(jp, Player.class);
                System.out.println("Player Name: " + player.getName() + "\nPlayer E-mail:" + player.getEmailAddress() + "\n");
            }
        } catch (IOException ioe) {
            System.out.println("Error parsing json array");
        }
    }

    /**
     * This method will access the CamperService to get all of the campers from
     * the campers web application service.
     *
     * @since 20161115
     * @author BJM
     */
}
