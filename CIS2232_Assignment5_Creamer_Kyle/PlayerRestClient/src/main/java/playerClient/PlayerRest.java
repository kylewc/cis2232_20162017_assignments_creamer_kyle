package playerClient;

import java.util.Scanner;

/**
 *
 * @author Elkeno
 */
public class PlayerRest {
    
    final public static String MENU =
            "V) View Players\n"
            + "X) eXit";
    final static Scanner input = new Scanner(System.in);
    
    public static void main(String[] args)
    {
        boolean endProgram = false;
        do{
            System.out.println(MENU);
            String choice = input.nextLine();
            
            switch(choice.toUpperCase()){
                case "V":
                    RestMethods.listPlayersUsingRest();
                    break;
                case "X":
                    endProgram = true;
                    break;
                default:
                    System.out.println("INVALID OPTION");
            }
        }while(!endProgram);
    }
    
    
}
