/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package info.hccis.player.web.rest;

import info.hccis.player.data.springjpa.PlayerRepository;
import info.hccis.player.entity.Player;
import java.io.IOException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

/**
 *
 * @author Kyle
 */

@Path("PlayerService")
public class PlayerRESTFacade {

    @Resource
    private final PlayerRepository playerRepository;

    public PlayerRESTFacade(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.playerRepository = applicationContext.getBean(PlayerRepository.class);
    }

    @GET
    @Path("/hello")
    public Response hello() {

        System.out.println("in controller for /player/hello");
        return Response.status(200).entity("hello").build();
    }

    @GET
    @Path("/players")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {

        System.out.println("in controller for /camper/list");
        ArrayList<Player> list = (ArrayList<Player>) playerRepository.findAll();
        
        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(list);
        } catch (IOException ex) {
            Logger.getLogger(PlayerRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * GET operation which will return a specific camper based on the id passed in.
     * @param id
     * @return json representing the camper.
     */
    
    @GET
    @Path("players/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        System.out.println("in controller for /camper/list");
        Player player = playerRepository.findOne(id);
        if(player == null){
            return Response.status(204).entity("{}").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(player);
        } catch (IOException ex) {
            Logger.getLogger(PlayerRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * POST operation which will update a camper.
     * @param jsonIn 
     * @return response including the json representing the new camper.
     * @throws IOException 
     */
    
    @POST
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

        ObjectMapper mapper = new ObjectMapper();
        
        //JSON from String to Object

       Player player = mapper.readValue(jsonIn, Player.class);
        player = playerRepository.save(player);
        String temp = "";
        try {
            temp = mapper.writeValueAsString(player);
        } catch (IOException ex) {
            Logger.getLogger(PlayerRESTFacade.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(201).entity(temp).build();
    }

}
