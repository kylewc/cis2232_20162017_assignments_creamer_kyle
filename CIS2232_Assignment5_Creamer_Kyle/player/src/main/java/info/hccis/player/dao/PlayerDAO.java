package info.hccis.player.dao;

import info.hccis.player.entity.Player;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.ArrayList;
import java.util.logging.Logger;

/**
 * This class will contain db functionality for working with players
 *
 * @author bjmaclean
 * @since 20160929
 */
public class PlayerDAO {

    private final static Logger LOGGER = Logger.getLogger(PlayerDAO.class.getName());

    public static Player select(int idIn) {

        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        Player thePlayer= null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM player where id = ?";

            ps = conn.prepareStatement(sql);
            ps.setInt(1, idIn);
            ResultSet rs = ps.executeQuery();

            while (rs.next()) {

                int id = rs.getInt("id");
                String playerName = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phoneNumber = rs.getString("phoneNumber");
                String emailAddress = rs.getString("emailAddress");
                int amountPaid = rs.getInt("amountPaid");
                Player temp = new Player();
                temp.setId(id);
                temp.setName(playerName);
                temp.setParentName(parentName);
                temp.setPhoneNumber(phoneNumber);
                temp.setEmailAddress(emailAddress);
                temp.setAmountPaid(amountPaid);

                thePlayer = temp;
            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return thePlayer;
    }

    /**
     * Select all players
     *
     * @author bjmaclean
     * @since 20160929
     * @return
     */
    public static ArrayList<Player> selectAll() {

        ArrayList<Player> thePlayers = new ArrayList<>();
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        try {
            conn = ConnectionUtils.getConnection();

            sql = "SELECT * FROM player order by id";

            ps = conn.prepareStatement(sql);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {

                int id = rs.getInt("id");
                String playerName = rs.getString("name");
                String parentName = rs.getString("parentName");
                String phoneNumber = rs.getString("phoneNumber");
                String emailAddress = rs.getString("emailAddress");
                int amountPaid = rs.getInt("amountPaid");
                Player temp = new Player();
                temp.setId(id);
                temp.setName(playerName);
                temp.setParentName(parentName);
                temp.setPhoneNumber(phoneNumber);
                temp.setEmailAddress(emailAddress);
                temp.setAmountPaid(amountPaid);
                thePlayers.add(temp);

            }
        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
        } finally {
            DbUtils.close(ps, conn);
        }
        return thePlayers;
    }

//    public static ArrayList<Player> selectAllByDOB(String year) {
//
//        ArrayList<Player> players = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM camper  where dob like '" + year + "' order by id";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//
//                int id = rs.getInt("id");
//                String firstName = rs.getString("firstName");
//                String lastName = rs.getString("lastName");
//                String dob = rs.getString("dob");
//                Player temp = new Player();
//                temp.setId(id);
//                temp.setFirstName(firstName);
//                temp.setLastName(lastName);
//                temp.setDob(dob);
//                players.add(temp);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return players;
//    }

    /**
     * This method will insert.
     *
     * @param player
     * @return
     * @author BJ
     * @since 20140615
     */
    public static synchronized Player update(Player player) throws Exception {
//        System.out.println("inserting player");
        PreparedStatement ps = null;
        String sql = null;
        Connection conn = null;
        

        /*
         * Setup the sql to update or insert the row.
         */
        try {
            conn = ConnectionUtils.getConnection();

            //Check to see if player exists.
            if (player.getId() == null || player.getId() == 0) {

                sql = "SELECT max(id) from player";
                ps = conn.prepareStatement(sql);
                ResultSet rs = ps.executeQuery();
                int max = 0;
                while (rs.next()) {
                    max = rs.getInt(1) + 1;
                }

                player.setId(max);

                sql = "INSERT INTO `player` (`id`, `name`, `parentName`, `phoneNumber`, `emailAddress`, `amountPaid`) "
                        + "VALUES (?,?,?,?,?,?)";

                ps = conn.prepareStatement(sql);
                ps.setInt(1, player.getId());
                ps.setString(2, player.getName());
                ps.setString(3, player.getParentName());
                ps.setString(4, player.getPhoneNumber());
                ps.setString(5, player.getEmailAddress());
                ps.setDouble(6, player.getAmountPaid());

            } else {

                sql = "UPDATE `Camper` SET `name`=?,`parentName`=?,`phoneNumber`=?, 'emailAddress'=?, 'amountPaid'=?, WHERE id = ?";

                ps = conn.prepareStatement(sql);
                ps.setString(1, player.getName());
                ps.setString(2, player.getParentName());
                ps.setString(3, player.getPhoneNumber());
                ps.setString(4, player.getEmailAddress());
                ps.setDouble(5, player.getAmountPaid());
                ps.setInt(6, player.getId());

            }
            /*
             Note executeUpdate() for update vs executeQuery for read only!!
             */

            ps.executeUpdate();

        } catch (Exception e) {
            String errorMessage = e.getMessage();
            e.printStackTrace();
            throw e;
        } finally {
            DbUtils.close(ps, conn);
        }
        return player;

    }

}

//    /**
//     * This method will insert.
//     *
//     * @return
//     * @author BJ
//     * @since 20140615
//     */
//    public static void insertNotification(Notification notification) throws Exception {
//        System.out.println("inserting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "INSERT INTO `notification`(`notification_type_code`,  "
//                    + "`notification_detail`, `status_code`, `created_date_time`, "
//                    + "`created_user_id`, `updated_date_time`, `updated_user_id`) "
//                    + "VALUES (?, ?, 1, sysdate(), ?, sysdate(), ?)";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notification.getNotificationType());
//            ps.setString(2, notification.getNotificationDetail());
//            ps.setString(3, notification.getUserId());
//            ps.setString(4, notification.getUserId());
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//
//    /**
//     * Delete the specified member education (set to inactive)
//     * @param memberId
//     * @param memberEducationSequence 
//     */
//    public static void deleteNotification(int notificationId) throws Exception{
//        
//        System.out.println("deleting notification");
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//
//        /*
//         * Setup the sql to update or insert the row.
//         */
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "update notification set status_code = 0, updated_date_time = sysdate() "
//                + "where notification_id = ? ";
//
//            ps = conn.prepareStatement(sql);
//            ps.setInt(1, notificationId);
//
//            ps.executeUpdate();
//
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//            throw e;
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return;
//
//    }
//        
//
//    
//    public static ArrayList<Notification> getNotifications() {
//        ArrayList<Notification> notifications = new ArrayList();
//        PreparedStatement ps = null;
//        String sql = null;
//        Connection conn = null;
//        try {
//            conn = ConnectionUtils.getConnection();
//
//            sql = "SELECT * FROM notification WHERE status_code = 1 order by created_date_time desc";
//
//            ps = conn.prepareStatement(sql);
//            ResultSet rs = ps.executeQuery();
//            while (rs.next()) {
//                Notification newNotification = new Notification();
//                newNotification.setNotificationId(rs.getInt("notification_id"));
//                newNotification.setNotificationDetail(rs.getString("notification_detail"));
//                newNotification.setNotificationType(rs.getInt("notification_type_code"));
//                newNotification.setNotificationDate(rs.getString("created_date_time"));
//                newNotification.setUserId(rs.getString("created_user_id"));
//                notifications.add(newNotification);
//            }
//        } catch (Exception e) {
//            String errorMessage = e.getMessage();
//            e.printStackTrace();
//        } finally {
//            DbUtils.close(ps, conn);
//        }
//        return notifications;
//    }

