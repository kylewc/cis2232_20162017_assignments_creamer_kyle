package info.hccis.camper.web.soap;

import info.hccis.camper.dao.CamperDAO;
import javax.jws.WebService;
import javax.jws.WebMethod;
import javax.jws.WebParam;

/**
 *
 * @author bjmaclean
 */
@WebService(serviceName = "Camper")
public class Camper {

    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "hello")
    public String hello(@WebParam(name = "name") String txt) {
        return "Hello " + txt + " !";
    }
    
    /**
     * This is a sample web service operation
     */
    @WebMethod(operationName = "get")
    public info.hccis.camper.entity.Camper getCamper(@WebParam(name = "id") String id) {
        return CamperDAO.select(Integer.parseInt(id));
    }
    
}
