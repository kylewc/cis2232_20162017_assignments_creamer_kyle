package info.hccis.camper.web;

import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.Camper;
import info.hccis.camper.entity.UserAccess;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class CamperController {

    private final CamperRepository camperRepository;

    @Autowired
    public CamperController(CamperRepository camperRepository) {
        this.camperRepository = camperRepository;
    }

    @RequestMapping("/camper/add")
    public String camperAdd(Model model, HttpSession session) {
        
        UserAccess user = (UserAccess) session.getAttribute("user");
        if(user == null || user.getUserType() == 0) {
        UserAccess user2 = new UserAccess();
        user2.setUsername("");
        model.addAttribute("user", user2);
            return "/camper/welcome";
        }
        
        
        System.out.println("BJM-in controller for /camper/add");
        Camper temp = new Camper();
        model.addAttribute("camper", temp);
        
        
        //TESTING adding strings to collection which could be used for custom errors.
        //http://stackoverflow.com/questions/33106391/how-to-check-if-list-is-empty-using-thymeleaf
        ArrayList<String> businessErrors = new ArrayList();
        businessErrors.add("Test1");
        businessErrors.add("Test2");
        model.addAttribute("businessErrors", businessErrors);
        
        
        
        return "camper/add";
    }

    @RequestMapping("/camper/edit")
    public String camperAddSubmit(Model model, HttpServletRequest request) {
        int id = Integer.parseInt(request.getParameter("id"));
        System.out.println("BJM-/camper/edit -->id=" + id);
        
        //get the camper from the repository based on the id
        Camper temp = camperRepository.findOne(id);
        //send the user back to the add page after putting the camper object in the model
        model.addAttribute("camper", temp);
        return "camper/add";
        
       
    }

    @RequestMapping("/camper/addSubmit")
    public String camperAddSubmit(Model model, @Valid @ModelAttribute("camper") Camper camper, BindingResult result, HttpSession session) {

        //check that they are logged in.
        UserAccess user = (UserAccess) session.getAttribute("user");
        if(user == null || user.getUserType() == 0) {
            return "/camper/welcome";
        }
        
        
        
        if (result.hasErrors()) {
            System.out.println("Error in validation of camper.");
            
                    //TESTING adding strings to collection which could be used for custom errors.
            //http://stackoverflow.com/questions/33106391/how-to-check-if-list-is-empty-using-thymeleaf
        ArrayList<String> businessErrors = new ArrayList();
        businessErrors.add("Test1");
        businessErrors.add("Test2");
        model.addAttribute("businessErrors", businessErrors);
            
            return "/camper/add";
        }
        
        
        try {
            //System.out.println("camper name =" + camper.getFirstName());

               // camper.setId(1);
            camperRepository.save(camper);
            
//            CamperDAO.update(camper);
            //send to the list of campers view.
            //ArrayList<Camper> campers = CamperDAO.selectAll();
            ArrayList<Camper> campers = (ArrayList<Camper>) camperRepository.findAll();
            HashMap<Integer, String> camperTypeMap = (HashMap<Integer, String>) session.getAttribute("CampTypesMap");
            for(Camper camperLoop: campers){
                camperLoop.setCampTypeDescription(camperTypeMap.get(camperLoop.getCampType()));
            }
            model.addAttribute("theCampers", campers);

            return "/camper/list";
        } catch (Exception ex) {
            System.out.println("There was an error adding the camper.");
            return "";
        }
    }
}
