package info.hccis.camper.web.rest;

import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.Camper;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.annotation.Resource;
import javax.servlet.ServletContext;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import org.codehaus.jackson.map.ObjectMapper;
import org.springframework.context.ApplicationContext;
import org.springframework.web.context.support.WebApplicationContextUtils;

@Path("CamperService")
public class CamperRest {

    @Resource
    private final CamperRepository camperRepository;

    public CamperRest(@Context ServletContext servletContext) {
        ApplicationContext applicationContext = WebApplicationContextUtils.getWebApplicationContext(servletContext);
        this.camperRepository = applicationContext.getBean(CamperRepository.class);
    }

    @GET
    @Path("/hello")
    public Response hello() {

        System.out.println("in controller for /camper/hello");
        return Response.status(200).entity("hello").build();
    }

    @GET
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    public Response list() {

        System.out.println("in controller for /camper/list");
        ArrayList<Camper> list = (ArrayList<Camper>) camperRepository.findAll();

        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(list);
        } catch (IOException ex) {
            Logger.getLogger(CamperRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * GET operation which will return a specific camper based on the id passed
     * in.
     *
     * @param id
     * @return json representing the camper.
     */
    @GET
    @Path("campers/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response get(@PathParam("id") int id) {

        System.out.println("in controller for /camper/list");
        Camper camper = camperRepository.findOne(id);
        if (camper == null) {
            return Response.status(204).entity("{}").build();
        }
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(camper);
        } catch (IOException ex) {
            Logger.getLogger(CamperRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    @GET
    @Path("/campersAllergyGluten")
    @Produces(MediaType.APPLICATION_JSON)
    public Response listAllergies() {

        System.out.println("in controller for /camper/list");
        ArrayList<Camper> list = (ArrayList<Camper>) camperRepository.findAll();
        CharSequence allergies = "gluten";

        Iterator<Camper> iter = list.iterator();

        while (iter.hasNext()) {
            Camper camper = iter.next();

            if (camper.getAllergies() == null || !(camper.getAllergies().toLowerCase().contains(allergies))) {
                iter.remove();
            }
        }

        //************************************************
        // Use classes from the Jackson library to convert our
        // array list of objects to json.
        //*************************************************
        final ObjectMapper mapper = new ObjectMapper();
        String temp = "";
        try {
            temp = mapper.writeValueAsString(list);
        } catch (IOException ex) {
            Logger.getLogger(CamperRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(200).entity(temp).build();
    }

    /**
     * POST operation which will update a camper.
     *
     * @param jsonIn
     * @return response including the json representing the new camper.
     * @throws IOException
     */
    @POST
    @Path("/campers")
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public Response updateCamper(String jsonIn) throws IOException {

        ObjectMapper mapper = new ObjectMapper();

        //JSON from String to Object
        Camper camper = mapper.readValue(jsonIn, Camper.class);
        camper = camperRepository.save(camper);
        String temp = "";
        try {
            temp = mapper.writeValueAsString(camper);
        } catch (IOException ex) {
            Logger.getLogger(CamperRest.class.getName()).log(Level.SEVERE, null, ex);
        }
        return Response.status(201).entity(temp).build();
    }

}
