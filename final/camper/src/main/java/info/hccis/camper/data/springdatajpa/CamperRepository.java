package info.hccis.camper.data.springdatajpa;


import info.hccis.camper.entity.Camper;
import java.util.List;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CamperRepository extends CrudRepository<Camper, Integer> {
    List<Camper> findByFirstName(String firstName);

    @Query(value="select max(id) from Camper", nativeQuery = true)
    Integer selectMaxId();
    
}