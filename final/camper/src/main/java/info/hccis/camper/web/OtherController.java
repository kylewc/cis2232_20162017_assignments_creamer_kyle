package info.hccis.camper.web;

import info.hccis.camper.dao.CodeValueDAO;
import info.hccis.camper.dao.UserAccessDAO;
import info.hccis.camper.data.springdatajpa.CamperRepository;
import info.hccis.camper.entity.Camper;
import info.hccis.camper.entity.CodeValue;
import info.hccis.camper.entity.UserAccess;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class OtherController {

    private final CamperRepository camperRepository;

    @Autowired
    public OtherController(CamperRepository camperRepository) {
        this.camperRepository = camperRepository;
    }

//    @RequestMapping("/")
//    public String showHome(Model model) {
//        System.out.println("in controller for /");
//
//        ArrayList<Camper> campers = CamperDAO.selectAll();
//        System.out.println("Number of campers = " + campers.size());
//        model.addAttribute("theCampers", campers);
//
//        return "camper/list";
//    }
    @RequestMapping("/")
    public String showHome(Model model) {

        //Show all the people in the db
        ArrayList<Camper> people = (ArrayList<Camper>) camperRepository.findByFirstName("Nick");
        for (Camper camper : people) {
            System.out.println(camper.getFirstName() + " " + camper.getLastName());
        }

        System.out.println("max id=" + camperRepository.selectMaxId());

        UserAccess user = new UserAccess();
        user.setUsername("test");
        model.addAttribute("user", user);

        //TESTING passing this to the html page to test showing it in a message.
        model.addAttribute("testNumber", "5");
        return "camper/welcome";
    }

    @RequestMapping("/authenticate")
    public String authenticate(Model model, @ModelAttribute("user") UserAccess user, HttpServletRequest request, HttpSession session) {

        /*
        
        See the @ModelAttribute refers to the object that is used on the form.  This is
        obtained here in the controller.  This is a common way that the controller gets access
        to the object and its attributes.  
        
         */
//Test to show what was passed in.  
        System.out.println("BJM in /authenticate, username passed in to controller is:" + user.getUsername());

        //Check for access of the user
        String accessLevel;
        if (user.getUsername() == null || user.getUsername().equals("test")) {
            accessLevel = "1";
            user.setUserType(1);
        } else {
            accessLevel = UserAccessDAO.getUserTypeCode(user.getUsername(), user.getPassword());
            user.setUserType(Integer.parseInt(accessLevel));
        }
        if (accessLevel.equals("0")) {
            return "/camper/welcome";
        } else {

            //        Here we will store the user in the session.  It will then be available to 
            //        other future requests.  
            request.getSession().setAttribute("user", user);

            //Setup the Camp Types
            ArrayList<CodeValue> campTypes = (ArrayList<CodeValue>) CodeValueDAO.getCodeValues("2");
            HashMap<Integer, String> campTypesMap = new HashMap();
            for (CodeValue cv : campTypes) {
                campTypesMap.put(cv.getCodeValueSequence(), cv.getEnglishDescription());
            }

            //Store this in the session
            session.setAttribute("CampTypes", campTypes);
            session.setAttribute("CampTypesMap", campTypesMap);

            //Set an object in the model to be used on the next form.  
            ArrayList<Camper> campers = (ArrayList<Camper>) camperRepository.findAll();
            for (Camper camperLoop : campers) {
                camperLoop.setCampTypeDescription(campTypesMap.get(camperLoop.getCampType()));
            }
            //ArrayList<Camper> campers = CamperDAO.selectAll();
            System.out.println("Number of campers = " + campers.size());
            model.addAttribute("theCampers", campers);

            /*            
        Specify the return.  This is the next view that will be presented to the user.
             */
            return "/camper/list";
        }
    }

    @RequestMapping("/newOne")
    public String showNewOne(Model model) {
        System.out.println("in controller for /newOne");
        return "camper/newOne";

        /*
        After this have to make sure that this html page exists. This would be 
        in the WEB-INF/thymeleaf/camper/
         */
    }

}
