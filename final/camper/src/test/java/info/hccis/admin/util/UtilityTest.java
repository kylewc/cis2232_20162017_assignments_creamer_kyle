package info.hccis.admin.util;

import java.util.logging.Level;
import java.util.logging.Logger;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author bjmaclean
 */
public class UtilityTest {

    public UtilityTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        System.out.println("doing some setup if necessary...");
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() {
    }

    @After
    public void tearDown() {
    }

    @Test
    public void testGetMD5HashFor123Guess() {
        String md5 = "";
        try {
            md5 = info.hccis.camper.util.Utility.getMD5Hash("123");
            assert md5.equals("202cb962ac59075b964b07152d234b70");
        } catch (Exception ex) {
            Logger.getLogger(UtilityTest.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}


